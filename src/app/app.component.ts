import {Component, HostListener} from '@angular/core';
import {GoogleLoginProvider, SocialAuthService} from "angularx-social-login";
import {User} from "./models/user.model";
import {TagsService} from "./services/tags.service";
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'news-aggregator';

  loggedUser: User | null = null;
  isLoggedIn: boolean = false;
  showLogoutCard: boolean = false;
  showTagsList: boolean = true;

  constructor(
    private socialAuthService: SocialAuthService,
    private showTagsService: TagsService,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    this.socialAuthService.authState.subscribe((user) => {
      this.loggedUser = new User(user.name, user.email, user.photoUrl);
      this.isLoggedIn = true;
      this.userService.changeUserState(this.loggedUser);
    });
    this.showTagsService.showTagsSubject.next(this.showTagsList);
  }

  showTags() {
    this.showTagsList = !this.showTagsList;
    this.showTagsService.showTagsSubject.next(this.showTagsList);
  }

  logoutCardToggler($event: any) {
    this.showLogoutCard = !this.showLogoutCard
    $event.stopPropagation()
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  logOut(): void {
    this.socialAuthService.signOut().then(() =>
      this.userService.changeUserState(null)
    );
    this.loggedUser = null;
    this.isLoggedIn = false;
    this.showLogoutCard = false;
  }

  @HostListener('document:click', ['$event']) onDocumentClick() {
    this.showLogoutCard = false;
  }

}
