import {News} from "../models/news.model";
import {Tag} from "../models/tag.model";
import {NewsComment} from "../models/news-comment.model";

export class TempData {

  public static tagsArray = [
    new Tag(0, "nalesniki"),
    new Tag(1, "gumisie"),
    new Tag(2, "jagody"),
    new Tag(3, "kibelek"),
    new Tag(4, "sport"),
    new Tag(5, "pogoda"),
    new Tag(6, "finanse"),
    new Tag(7, "ekonomia"),
    new Tag(8, "programowanie"),
    new Tag(9, "algorytmy"),
  ]

  public static commentsArray = [
    new NewsComment(1, "tadzik@samjestes.com", "tadzik mysli ze gitowa pioerjgiopeqr joqehr goerjgop jeropig joprehg oerh gpowhrpioughj rtskh io;erwstjko;wrth klaterhg o;aerhjgo;aer joihjrt ohjsrtyop jsrto; jwrte[ oihjwrtiopu  ghjbjk;fgdjnq45oiejgpisfgj hrtasioeyh jwr5io hmo;sfg jwrotih josrtij hbklgsjf noprtis jwrtioe jio[wert j"),
    new NewsComment(2, "chyba@ty.com", "ogryzek z jabłka ioj iopuaerhj iopuqehg ioperh pohe rphe proiuh iopueh o[pj ["),
    new NewsComment(3, "awcale@ze.tak", "kaszana, taka z majonezem eriu jpiouer iopuer hipe4hr qerop hgopqe5hgoph qeroj r[pj [perj o[j3 4oe[jae er "),
    new NewsComment(4, "moze@lubi.banany", "złota polska młodziez kradnie ople re[oi gjqero;i hje rpoigjeropqi jowrthj odf gjiopuerth h "),
    new NewsComment(5, "pociagi@jada.sobie", "opole to ja chromole reajgopi aehrp ghioer ghpuiower ghjpeoq5yhj werjero[ hjwr[toh j"),
    new NewsComment(6, "alemja@tralalala.com", "no pinknie, ku... pinknie oerjgh l;iwrtej hoiwrtj hiowrjt pouihgjr twipghjoer jgwrt hwrt hjr j"),
  ]

  public static newsArray = [
    new News(1, "super news", new Date(2022, 3, 12), "https://www.reuters.com/technology/elon-musk-offers-buy-twitter-5420-per-share-2022-04-14/", [TempData.tagsArray[0], TempData.tagsArray[1]], "this is part of some super news oiaerughio laruweghip uaerhgiou erhgouiphaerl uifghaeri lufhuq3e4f guowehfgi aer huiyerawgf iuerahgiou haerioug herauio ghiuaergh ioae4rh goaergjpiowEJFO;VERHA9[ GJSERPAIUGH OP'EARJGPIQERUJ GOIAEHJROP GIW A;OGHWE4R95PO GJR;AOEIHG O;IS HPIO5ERUOIJ", [TempData.commentsArray[0], TempData.commentsArray[1]], "photo1"),
    new News(2, "gruba akcja", new Date(2022, 3, 2), "https://edition.cnn.com/2022/04/14/us/brooklyn-subway-shooting-thursday/index.html", [TempData.tagsArray[1], TempData.tagsArray[2]], "second super news", [TempData.commentsArray[1], TempData.commentsArray[2]], "photo2"),
    new News(3, "wąchocka ustawka", new Date(2022, 3, 14), "https://eu.usatoday.com/story/news/politics/2022/04/14/ukraine-russia-invasion-live-updates/7314494001/", [TempData.tagsArray[2], TempData.tagsArray[3]], "third news not so good", [TempData.commentsArray[2], TempData.commentsArray[3]], "photo3"),
    new News(4, "misie gumisi", new Date(2022, 2, 12), "https://www.nytimes.com/2022/04/13/us/politics/biden-weapons-ukraine.html", [TempData.tagsArray[3], TempData.tagsArray[4]], "so f.... ugly", [TempData.commentsArray[3], TempData.commentsArray[4]], "photo4"),
    new News(5, "tadzik wzywa misie", new Date(2022, 5, 12), "https://www.cnbc.com/2022/04/14/russia-threatens-new-nuclear-deployments-if-sweden-finland-join-nato.html", [TempData.tagsArray[4], TempData.tagsArray[0]], "not feeling very well", [TempData.commentsArray[4], TempData.commentsArray[5]], "photo5"),
  ]

}


