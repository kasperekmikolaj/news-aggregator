import {Tag} from "./tag.model";
import {NewsComment} from "./news-comment.model";

export class News {
  constructor(
    public id: number,
    public title: string,
    public date: Date,
    public link: string,
    public tags: Tag[],
    public textPart: string,
    public comments: NewsComment[],
    public photo: string,
    public showComments: boolean = false,
  ) {
  }
}
