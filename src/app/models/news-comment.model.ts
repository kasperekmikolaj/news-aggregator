export class NewsComment {

  constructor(
    public id: number,
    public authorEmail: string,
    public text: string,
  ) {
  }
}
