export class WeatherDay{

  constructor(
    public dayOfWeek: string,
    public temperatureDay: number,
    public temperatureNight: number,
    public icon: string,
  ) {
  }
}
