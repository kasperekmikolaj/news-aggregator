import {Component, OnInit} from '@angular/core';
import {WeatherService} from "../../services/weather.service";
import {WeatherDay} from "../../models/weather-day.model";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  private latitude: number = 51.1079;
  private longitude: number = 17.0385;
  city: string = "";
  days: WeatherDay[] = [];
  weatherData: any = null;

  constructor(
    private weatherService: WeatherService,
  ) {
  }

  ngOnInit(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.getForecastData();
      });
    } else {
      this.getForecastData();
    }
  }

  getForecastData() {
    this.weatherService.getForecast(this.latitude, this.longitude).subscribe((data) => {
      // ikona, temp rano i noc, dzien tygodnia,
      for (let i = 0; i < data.daily.length; i++) {
        let daily = data.daily[i];

        let day = this.capitalize(new Date(daily.dt * 1000).toLocaleDateString(undefined, {weekday: "short",}));
        let tempDay = Math.round(daily.temp.day);
        let tempNight = Math.round(daily.temp.night);

        let clouds = parseInt(daily.clouds);
        let rain = daily.rain;
        let snow = daily.snow;

        let icon = ""
        if (snow != null) {
          icon = "wi-snow"
        } else if (rain != null) {
          icon = "wi-rain"
        } else if (clouds > 50) {
          icon = "wi-cloudy"
        } else {
          icon = "wi-day-sunny"
        }
        this.days.push(new WeatherDay(day, tempDay, tempNight, icon))
      }
      this.weatherData = data;
    });
    this.weatherService.getLocation(this.latitude, this.longitude).subscribe((data) => {
      this.city = data.city !== "" ? data.city : data.locality;
    });
  }

  capitalize(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }
}
