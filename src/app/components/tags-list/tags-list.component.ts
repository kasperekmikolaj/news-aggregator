import {Component, Input, OnInit} from '@angular/core';
import {Tag} from "../../models/tag.model";
import {TagsService} from "../../services/tags.service";

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.css']
})
export class TagsListComponent implements OnInit {

  @Input()
  tagsList: Tag[] = [];

  constructor(
    private tagsService: TagsService,
  ) { }

  ngOnInit(): void {}

  onTagClick(tag: Tag, event: any) {
    event.preventDefault();
    this.tagsService.tagSelectedSubject.next(tag.id);
  }

  allClicked(event:any){
    event.preventDefault();
    this.tagsService.allTagsSubject.next();
  }
}
