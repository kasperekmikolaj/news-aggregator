import {Component, OnDestroy, OnInit} from '@angular/core';
import {TagsService} from "../../services/tags.service";
import {TagsListService} from "../../services/tags-list.service";
import {Tag} from "../../models/tag.model";
import {News} from "../../models/news.model";
import {NewsService} from "../../services/news.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user.model";
import {NewsComment} from "../../models/news-comment.model";
import {NewsCommentService} from "../../services/news-comment.service";
import {computeMsgId} from "@angular/compiler";


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit, OnDestroy {

  showTagsList: boolean = true;
  tagsList: Tag[] = [];
  newsList: News[] = [];
  numberOfPages: number = 1;
  pageNumberCollection: any = [];
  showStartRedirect = false;
  showEndRedirect = false;
  private NEWS_PER_PAGE = 20;
  currentPageNumber = -1;
  currentUser: User | null = null;
  addCommentText: string = "";

  constructor(
    private showTagsService: TagsService,
    private tagsListService: TagsListService,
    private newsListService: NewsService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private newsCommentService: NewsCommentService,
  ) {
  }

  ngOnInit(): void {
    this.showTagsService.showTagsSubject.subscribe((show: boolean) => {
      this.showTagsList = show;
    });
    this.showTagsService.tagSelectedSubject.subscribe((tagId: number) => {
      this.newsListService.getNewsFilteredByTag(tagId).subscribe((news: News[]) => {
        this.newsList = news;
      });
    });
    this.showTagsService.allTagsSubject.subscribe(() => {
      this.newsListService.getNews(this.currentPageNumber).subscribe((newsList) => {
        this.newsList = newsList;
      });
    });
    this.tagsListService.getTagsList().subscribe((tags: Tag[]) => {
      this.tagsList = tags;
    });

    this.route.params.subscribe(params => {
      this.currentPageNumber = params["pageNum"];

      this.newsListService.getNews(this.currentPageNumber).subscribe((newsList) => {
        this.newsList = newsList;
      });

      this.newsListService.getNumberOfNews().subscribe((newsCount) => {
        this.numberOfPages = Math.floor(newsCount / this.NEWS_PER_PAGE) + 1;

        if (this.numberOfPages > 21 && this.currentPageNumber > 11) {
          this.showStartRedirect = true;
        }
        if (this.numberOfPages > 21 && this.currentPageNumber < (this.numberOfPages - 11)) {
          this.showEndRedirect = true;
        }

        if(this.currentPageNumber < 11) {
          this.pageNumberCollection = Array(20).fill(1).map((x, i) => i);
        } else if (this.currentPageNumber > this.numberOfPages-12){
          this.pageNumberCollection = Array(20).fill(1).map((x, i) => this.numberOfPages-20 + i);
        } else {
          this.pageNumberCollection = Array(20).fill(1).map((x, i) => this.currentPageNumber-11 + i);
        }
      });
    });

    this.currentUser = this.userService.getCurrentUser();
    this.userService.getUserSubjectForSubscription().subscribe((user) => {
      this.currentUser = user;
    });
  }

  ngOnDestroy(): void {
    this.showTagsService.showTagsSubject.unsubscribe();
  }

  showComments(event: any, news: News) {
    event.preventDefault();
    news.showComments = !news.showComments;
  }

  changePage(pageNum: number, event: any) {
    event.preventDefault();
    if(pageNum != this.currentPageNumber) {
      this.router.navigate(["/" + pageNum.toString()])
    }
  }

  addNewComment(news: News){
    this.newsCommentService.postNewComment(this.currentUser!.email, news.id, this.addCommentText).subscribe((comment:NewsComment) => {
      news.comments.push(comment);
    });
    this.addCommentText = '';
  }

  deleteComment(news: News, id: number) {
    this.newsCommentService.deleteComment(id).subscribe(() => {
      let index = news.comments.map(function (comment) {return comment.id}).indexOf(id);
      news.comments.splice(index, 1);
    })
  }

}
