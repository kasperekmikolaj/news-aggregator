import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { WeatherComponent } from './components/weather/weather.component';
import { TagsListComponent } from './components/tags-list/tags-list.component';
import { NewsListComponent } from './components/news-list/news-list.component';
import { ListItemComponent } from './components/news-list/list-item/list-item.component';
import { NewsDetailComponent } from './components/news-detail/news-detail.component';
import { NewsCommentListComponent } from './components/news-detail/news-comment-list/news-comment-list.component';
import { NewsCommentComponent } from './components/news-detail/news-comment-list/news-comment/news-comment.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import {HttpClientModule} from "@angular/common/http";
import {ShareButtonsModule} from "ngx-sharebuttons/buttons";

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    TagsListComponent,
    NewsListComponent,
    ListItemComponent,
    NewsDetailComponent,
    NewsCommentListComponent,
    NewsCommentComponent,
    MainPageComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        SocialLoginModule,
        HttpClientModule,
        FormsModule,
        ShareButtonsModule,
    ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('823504960573-h50td9eav20s6v0mc2b6o9b24m57l5f1.apps.googleusercontent.com')
          }
        ]
    } as SocialAuthServiceConfig,
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
