import {Injectable} from "@angular/core";
import {User} from "../models/user.model";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private loggedUser: User | null = null;
  private loggedUserSubject: Subject<User | null> = new Subject<User | null>();

  changeUserState(user: User | null){
    this.loggedUser = user;
    this.loggedUserSubject.next(user);
  }

  getCurrentUser() {
    return this.loggedUser;
  }

  getUserSubjectForSubscription() {
    return this.loggedUserSubject;
  }

}
