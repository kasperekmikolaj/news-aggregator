import {Injectable} from "@angular/core";
import {from, Observable} from "rxjs";
import {Tag} from "../models/tag.model";
import {HttpClient} from "@angular/common/http";
import {TempData} from "../tempData/tempData";

@Injectable({
  providedIn: 'root'
})
export class TagsListService {

  constructor(
    private http: HttpClient,
  ) {
  }

  // fixme
  public getTagsList(): Observable<Tag[]> {
    // return this.http.get<Tag[]>(Constants.BACKEND + "tags");
    return from([TempData.tagsArray]);
  }

}
