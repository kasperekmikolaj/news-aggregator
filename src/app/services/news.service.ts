import {Injectable} from "@angular/core";
import {from, Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {TempData} from "../tempData/tempData";
import {Constants} from "../constants";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(
    private http: HttpClient,
  ) {}

  getNews(pageNumber: number): Observable<any>{
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber.toString())
    // fixme
    // return this.http.get(Constants.BACKEND + "news/", {params})
    return from([TempData.newsArray]);
  }

  getNewsFilteredByTag(tagId: number): Observable<any>{
    // fixme
    // return this.http.get(Constants.BACKEND + "news/" + tagId.toString())
    if(tagId%2 === 0) {
      return from([TempData.newsArray.slice(2)]);
    } else {
      return from([TempData.newsArray.slice(4)]);
    }
  }

  getNumberOfNews():Observable<any> {
    // fixme
    // return this.http.get(Constants.BACKEND + "news/number");
    return from([14000]);
  }

}
