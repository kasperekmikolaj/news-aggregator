import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private weatherUrl = "https://api.openweathermap.org/data/2.5/onecall";
  private apiKey = "ea812b4173e3b7879ed73b3cc5ef1b3c";

  private locationUrl = "https://api.bigdatacloud.net/data/reverse-geocode-client"

  constructor(
    private http: HttpClient,
    ) {}

  getForecast(latitude: number, longitude: number): Observable<any>{
    let params = new HttpParams();
    params = params.set('lat', latitude.toString())
    params = params.set('lon', longitude.toString())
    params = params.set('appid', this.apiKey)
    params = params.set('units', "metric")
    params = params.set('exclude', "current,minutely,hourly,alerts")

    return this.http.get(this.weatherUrl, {params})
  }

  getLocation(latitude: number, longitude: number): Observable<any>{
    let params = new HttpParams();
    params = params.set('latitude', latitude.toString())
    params = params.set('longitude', longitude.toString())
    params = params.set('localityLanguage', "pl")

    return this.http.get(this.locationUrl, {params})
  }

}
