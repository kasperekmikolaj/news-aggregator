import {Injectable} from "@angular/core";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  showTagsSubject: Subject<boolean> = new Subject<boolean>();

  tagSelectedSubject: Subject<number> = new Subject<number>();

  allTagsSubject: Subject<void> = new Subject<void>();

}
