import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {from, Observable} from "rxjs";
import {TempData} from "../tempData/tempData";
import {Constants} from "../constants";
import {NewsComment} from "../models/news-comment.model";

@Injectable({
  providedIn: 'root'
})
export class NewsCommentService {

  constructor(
    private http: HttpClient,
  ) {}

  postNewComment(authorEmail: string, newsId: number, text: string): Observable<any>{
    let params = new HttpParams();
    params = params.set('authorEmail', authorEmail)
    params = params.set('newsId', newsId.toString())
    // fixme
    // return this.http.post(Constants.BACKEND + "comment/", {"text":text},{params})
    return from([new NewsComment(99, "kasperekmikolaj@gmail.com", "testowy comment")]);
  }

  deleteComment(commentId: number): Observable<any> {
    let params = new HttpParams();
    params = params.set('commentId', commentId.toString());
    // return this.http.delete(Constants.BACKEND + "comment/", {params});
    return from([[new NewsComment(99, "kasperekmikolaj@gmail.com", "testowy comment")]]);
  }

}
